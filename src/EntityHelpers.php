<?php

namespace Drupal\common_entity;

use Drupal\Component\Render\PlainTextOutput;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Utility\Token;
use Drupal\file\FileInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class EntityHelpers
 */
class EntityHelpers {

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * EntityHelpers constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   * @param \Drupal\Core\Utility\Token $token
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Database\Connection $database
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   */
  public function __construct(
    EntityFieldManagerInterface $entity_field_manager,
    Token $token,
    EntityTypeManagerInterface $entity_type_manager,
    Connection $database,
    FileSystemInterface $file_system
  ) {
    $this->entityFieldManager = $entity_field_manager;
    $this->token = $token;
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
    $this->fileSystem = $file_system;
  }

  /**
   * Creates image.
   *
   * @param string $type
   * @param string $path
   * @param string $entity_type
   * @param string $bundle
   * @param string $field_name
   * @param resource $connection
   *
   * @return int|boolean|null
   */
  public function createFileImage($type, $path, $entity_type, $bundle, $field_name, $connection = NULL) {
    $file_uri = $this->getTemporaryFile($type, $path, $connection);

    if (!$file_uri) {
      return NULL;
    }

    // If file is found locally.
    if (file_exists($file_uri)) {
      // If we already have file with same content.
      $destination = $this->getEntityImageFieldDestination($entity_type, $bundle, $field_name);
      $basename = basename($file_uri);
      $data = file_get_contents($file_uri);

      // Check the destination file path is writable.
      if (!$this->fileSystem->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY)) {
        throw new HttpException(500, 'Destination file path is not writable');
      }

      /** @var \Drupal\file\FileInterface $file */
      $file = file_save_data($data, $destination . '/' . $basename);

      if ($file instanceof FileInterface) {
        return $file->id();
      }
    }
  }

  /**
   * Get uri file.
   *
   * @param string $type
   * @param string $path
   * @param resource $connection
   *
   * @return mixed|null
   */
  private function getTemporaryFile($type, $path, $connection = NULL) {
    // Url.
    if ($type == 'url') {
      $host = parse_url($path);

      // If scheme is exists, then we treat this file as remote.
      if (isset($host['scheme'])) {
        $filename = basename($path);
        $uri = system_retrieve_file($path, 'temporary://' . $filename);

        return $uri;
      }
      else {
        return NULL;
      }
    }

    // FTP.
    if ($type == 'ftp') {
      $filename = basename($path);
      $uri = system_retrieve_file($path, 'temporary://' . $filename);

      if (ftp_get($connection, $uri, $path, FTP_BINARY)) {
        return $uri;
      }
      else {
        return NULL;
      }
    }
  }

  /**
   * Gets field destination value.
   *
   * @param string $entity_type
   * @param string $bundle
   * @param string $field_name
   *
   * @return array|string
   */
  private function getEntityImageFieldDestination($entity_type, $bundle, $field_name) {
    $result = &drupal_static(__CLASS__ . ':' . __METHOD__);

    if (!isset($result)) {
      $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
      $file_directory = trim($field_definitions[$field_name]->getSetting('file_directory'), '/');
      $uri_scheme = $field_definitions[$field_name]->getSetting('uri_scheme');

      // Since this setting can, and will be contain tokens by default. We must
      // handle it too. Also, tokens can contain html, so we strip it.
      $destination = PlainTextOutput::renderFromHtml($this->token->replace($file_directory));

      $result = $uri_scheme . '://' . $destination;
    }

    return $result;
  }

  /**
   * Get the number of nodes from the term.
   *
   * @param $tid
   *
   * @return mixed
   */
  public function getCountNodesFromTerm($tid) {
    $query = $this->database->select('taxonomy_index', 'ti');
    $query->addExpression('COUNT(DISTINCT ti.nid)');
    $query->condition('ti.tid', $tid);
    $query->addTag('vocabulary_node_count');

    return $query->execute()->fetchField();
  }

  /**
   * Get the number of entities from the term.
   *
   * @param $tid
   * @param $entity_type
   * @param $field_name
   *
   * @return mixed
   */
  public function getCountEntitiesFromTerm($tid, $entity_type, $field_name) {
    $query = $this->database->select($entity_type . '__' . $field_name, 'fd');
    $query->fields('fd');
    $query->condition($field_name . '_target_id', $tid);
    $query->condition('deleted', 0);

    return $query->countQuery()->execute()->fetchField();
  }

  /**
   * Get all entities from the term.
   *
   * @param array $tids
   * @param $entity_type
   * @param $field_name
   *
   * @return array|int
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getAllEntitiesFromTerm(array $tids, $entity_type, $field_name) {
    $query = $this->entityTypeManager->getStorage($entity_type)
      ->getQuery()
      ->condition($field_name, $tids, 'IN');

    return $query->execute();
  }
}
